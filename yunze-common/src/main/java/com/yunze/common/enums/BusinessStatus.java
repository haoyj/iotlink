package com.yunze.common.enums;

/**
 * 操作状态
 * 
 * @author yunze
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
