---
title: 程序部署
date: 2022-01-07
tags:
 - 说明
 - 部署
 - 帮助
categories:
 -  帮助
---

<div align="center">
 
<h1 >IoTLink</h1>

<h3 >基于 SpringBoot、Vue、Mybatis、RabbitMq、Mysql、Redis 的物联网卡管理系统</h3>

</div>



## 1 基础依赖安装 



### 1.1 服务器要求

建议服务器最低配置 [阿里云优惠券](https://www.aliyun.com/minisite/goods?userCode=3mwvbeng)：

| 类型 | 配置 |
|:-:|:-:|
|操作系统|CentOS Stream  8 64位|
|CPU|4核|
|内存|16G|
|带宽|5M|
|硬盘|100G|


## 1.2 程序依赖程序列表

| 循序 | 名称 | 版本 | 说明 |
|:-:|:-:|:-:|:-:|
|1|Mysql|8.0|最低5.7版本|
|2|Redis|6.2|缓存数据|
|3|Nginx|1.2.0|代理网站|
|4|RabbitMq|3.9.4|消息队列|
|5|JDK|1.8|Java程序依赖|


**推荐安装方式**


## 1.3 安装[宝塔](https://www.bt.cn)

::: tip 利用宝塔快捷安装
快捷安装 Mysql、Redis、Nginx、Docker
:::

> 这里讲解一个举例比方说你的linux 服务器 有多个盘 空间大的在 `/mnt` 或 `/home` (如没有其他盘跳至 3步骤)

**演示用 /mnt 举例**

 ```text
cd /mnt
```

1.创建宝塔面板安装需要用的www目录

 ```text
mkdir www
```

2.建立/mnt/www的软连接到/www （也就是给系统根目录建立一个www的“快捷方式”指向/mnt/www）

 ```text
cd /

ln -s /mnt/www /www
```

[宝塔官方安装教程](https://www.bt.cn/download/linux.html)

3.使用 SSH 连接工具，如堡塔SSH终端连接到您的 Linux 服务器后，挂载磁盘，根据系统执行相应命令开始安装（约2分钟完成）：

Centos安装脚本 `使用的是推荐的CentOS Stream 类型服务器直接用这个`

 ```text
yum install -y wget && wget -O install.sh http://download.bt.cn/install/install_6.0.sh && sh install.sh
```

Ubuntu/Deepin安装脚本

 ```text
wget -O install.sh http://download.bt.cn/install/install-ubuntu_6.0.sh && sudo bash install.sh
```

Debian安装脚本

 ```text
wget -O install.sh http://download.bt.cn/install/install-ubuntu_6.0.sh && bash install.sh
```

Fedora安装脚本

 ```text
wget -O install.sh http://download.bt.cn/install/install_6.0.sh && bash install.sh
```

> 注意：必须为没装过其它环境如Apache/Nginx/php/MySQL的新系统,推荐使用centos 7.X的系统安装宝塔面板
  

安装成功之后会打印出 你的服务器访问地址和账户密码 请妥善保管！另存

根据你的服务器宝塔地址访问登录，登录之后需要绑定账号，没有注册的注册一个账号


## 1.4 宝塔安装 Mysql、Redis、Nginx、Docker


找到目录 [软件商店] > 应用搜索 Mysql 找到最后右侧 操作 点击  `安装` ,安转完之后 打开首页展示 开关 

找到目录 [软件商店] > 应用搜索 Redis 找到最后右侧 操作 点击  `安装` ,安转完之后 打开首页展示 开关 

找到目录 [软件商店] > 应用搜索 Nginx 找到最后右侧 操作 点击  `安装` ,安转完之后 打开首页展示 开关 

找到目录 [软件商店] > 应用搜索 Docker 找到最后右侧 操作 点击  `安装` ,安转完之后 打开首页展示 开关 

如果您需要在服务器外其他服务器下运行 记得开放端口号！

**开放端口号**

[安全] > 防火墙 放行端口

 ```text
3306  MySQL服务默认端口	
6379  Redis
5672  RabbitMq
```



## 1.5 修改 mysql 配置 

找到目录 首页 > 左下角 软件 点击 MySQL 找到 `配置修改`

复制如下文件 粘贴 点击 `保存` 找到 服务 再 点击 `重启` mysql 

  ```text 
[client]
#password	= your_password
port		= 3306
socket		= /tmp/mysql.sock

[mysqld]
port		= 3306
socket		= /tmp/mysql.sock
datadir = /www/server/data
default_storage_engine = InnoDB
performance_schema_max_table_instances = 400
table_definition_cache = 400
skip-external-locking
key_buffer_size = 1024M
max_allowed_packet = 100G
table_open_cache = 4096
sort_buffer_size = 16M
net_buffer_length = 4K
read_buffer_size = 16M
read_rnd_buffer_size = 256K
myisam_sort_buffer_size = 256M
thread_cache_size = 512
tmp_table_size = 512M
default_authentication_plugin = mysql_native_password
lower_case_table_names = 1
sql-mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES

explicit_defaults_for_timestamp = true
#skip-name-resolve
max_connections = 500
max_connect_errors = 100
open_files_limit = 65535

log-bin=mysql-bin
binlog_format=mixed
server-id = 1
binlog_expire_logs_seconds = 600000
slow_query_log=1
slow-query-log-file=/www/server/data/mysql-slow.log
long_query_time=3
#log_queries_not_using_indexes=on
early-plugin-load = ""

innodb_data_home_dir = /www/server/data
innodb_data_file_path = ibdata1:10M:autoextend
innodb_log_group_home_dir = /www/server/data
innodb_buffer_pool_size = 4096M
innodb_log_file_size = 2048M
innodb_log_buffer_size = 512M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
innodb_max_dirty_pages_pct = 90
innodb_read_io_threads = 8
innodb_write_io_threads = 8

[mysqldump]
quick
max_allowed_packet = 500M

[mysql]
no-auto-rehash

[myisamchk]
key_buffer_size = 1024M
sort_buffer_size = 16M
read_buffer = 2M
write_buffer = 2M

[mysqlhotcopy]
interactive-timeout

```

## 1.6 修改 Redis 密码

开源项目默认  Redis 为 Rdis20220107

找到目录 首页 > 左下角 软件 点击 Redis 找到 `性能调整` > requirepass


将 requirepass 文本框 粘贴 `Rdis20220107` 点击 保存

如需 配置外网可访问 Redis  

修改配置文件中 bind 127.0.0.1 将其注释 点击 `保存` 找到 服务 再 点击 `重启` Redis 


 ```text
#bind 127.0.0.1
``` 





## 1.7 配置 Nginx

配置之前需要准备域名 如 demo.5iot.cn 到你的服务器 

找到目录 首页 > 左下角 软件 点击 Nginx 找到 `配置修改`


复制如下文件 粘贴 点击 `保存` 找到 服务 再 点击 `重启` Nginx 




 ```text
user  www www;
worker_processes auto;
error_log  /www/wwwlogs/nginx_error.log  crit;
pid        /www/server/nginx/logs/nginx.pid;
worker_rlimit_nofile 51200;

events
    {
        use epoll;
        worker_connections 51200;
        multi_accept on;
    }

http
    {
        include       mime.types;
		#include luawaf.conf;

		include proxy.conf;

        default_type  application/octet-stream;

        server_names_hash_bucket_size 512;
        client_header_buffer_size 32k;
        large_client_header_buffers 4 32k;
        client_max_body_size 50m;

        sendfile   on;
        tcp_nopush on;

        keepalive_timeout 60;

        tcp_nodelay on;

        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
        fastcgi_buffer_size 64k;
        fastcgi_buffers 4 64k;
        fastcgi_busy_buffers_size 128k;
        fastcgi_temp_file_write_size 256k;
		fastcgi_intercept_errors on;

        gzip on;
        gzip_min_length  1k;
        gzip_buffers     4 16k;
        gzip_http_version 1.1;
        gzip_comp_level 2;
        gzip_types     text/plain application/javascript application/x-javascript text/javascript text/css application/xml;
        gzip_vary on;
        gzip_proxied   expired no-cache no-store private auth;
        gzip_disable   "MSIE [1-6]\.";

        limit_conn_zone $binary_remote_addr zone=perip:10m;
		limit_conn_zone $server_name zone=perserver:10m;

        server_tokens off;
        access_log off;

server
    {
        listen 888;
        server_name phpmyadmin;
        index index.html index.htm index.php;
        root  /www/server/phpmyadmin;

        #error_page   404   /404.html;
        include enable-php.conf;

        location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
        {
            expires      30d;
        }

        location ~ .*\.(js|css)?$
        {
            expires      12h;
        }

        location ~ /\.
        {
            deny all;
        }

        access_log  /www/wwwlogs/access.log;
    }
    
    
     server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            #root   html;
            #index  index.html index.htm;
            root   admin;
            index  index.html;
        }
        
        
        location /prod-api/ {
					proxy_set_header Host $http_host;
					proxy_set_header X-Real-IP $remote_addr;
					proxy_set_header REMOTE-HOST $remote_addr;
					proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
					proxy_pass http://localhost:8080/;
				}
        
        

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
    
    
    
    server{
        listen       80;  #监听端口
        server_name demo.5iot.cn;

        location / {
            root admin;
            index  index.html;
            try_files $uri $uri/ /index.html; 
    	}
    	
    	location /prod-api/ {
					proxy_set_header Host $http_host;
					proxy_set_header X-Real-IP $remote_addr;
					proxy_set_header REMOTE-HOST $remote_addr;
					proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
					proxy_pass http://localhost:8080/;
				}
    	

   }
   
   
   
   
   
  
    
    
    
include /www/server/panel/vhost/nginx/*.conf;
}



```

## 1.8  查看Docker版本


**查看Docker版本**

> 默认无需执行

  ```text
# 简单信息
docker -v
# 查看docker的版本号，包括客户端、服务端、依赖的Go等
docker version
# 查看系统(docker)层面信息，包括管理的images, containers数等
docker info
```

**Docker服务相关**

> 默认无需执行

 ```text
# 启动
systemctl start docker
# 开机自启
systemctl enable docker
# 停止
systemctl stop docker
# 重启
systemctl restart docker
```

**查看docker状态**

  ```text
systemctl status docker
```

## 1.9  安装RabbitMQ 

**安装RabbitMQ 3.9.4 版本**

  ```text
docker pull rabbitmq:3.9.4-management
```

**查看镜像**

  ```text
docker images
```
![RabbitMq 查看镜像](../introduceImgs/docImg/RabbitMq_image.png)


**创建rabbitmq容器**

  ```text
docker run -d --restart=always --name test-rabbit -e RABBITMQ_DEFAULT_USER=test -e RABBITMQ_DEFAULT_PASS=test@2018 -p 15672:15672 -p 4369:4369 -p 5671-5672:5671-5672 -p 15671:15671 -p 25672:25672 rabbitmq:3.9.4-management
```

::: tip 注：参数说明
-p: 端口映射，其中5672：应用访问端口；15672：控制台Web端口号;

-v 数据卷挂载，需要完整路径；

RABBITMQ_DEFAULT_USER、RABBITMQ_DEFAULT_PASS：登录用户名和密码
:::



::: tip 查看容器是否运行

使用浏览器打开web管理端：http://Server-IP:15672

- Server-IP  = 你的 服务器 ip地址

这个时候输入 我们设置的 test test@2018 登录成功！
:::

![Mq登录成功](../introduceImgs/docImg/MqLoginSuccess.png)


## 1.10  安装 JDK

Jdk是1.8的版本

你需要一个JDK的压缩包 如 dk-8u271-linux-x64.tar.gz
[dk-8u271-linux-x64.tar.gz](https://www.jb51.net/softs/551521.html#downintro2)

**创建 /var/java 将下载的 jdk 上传至 文件夹 下**

  ```text
cd /

mkdir  /var/java
```

**解压**

  ```text
tar -zxvf jdk-8u271-linux-x64.tar.gz
```


**配置环境变量**

编辑 vim /etc/profile 加上

  ```text
vim /etc/profile
```

把下面这段 加在最后 上 用 VIM 编辑命令 [vim编辑教程](https://jingyan.baidu.com/article/6fb756ec72b031241858fb90.html)

  ```text
export JAVA_HOME=/var/java/jdk1.8.0_271
export CLASSPATH=.:$:CLASSPATH:$JAVA_HOME/lib/
export PATH=$PATH:$JAVA_HOME/bin
```

**退出保存后 刷新配置文件**

  ```text
source /etc/profile
```
**查看java版本号**

  ```text
java -version
```

> 如下显示 证明 安装成功

![JdkB版本号](../introduceImgs/docImg/java_version.png)



## 2 数据库配置

## 2.1 新建数据库

登录 宝塔 面板 查看数据库密码

::: tip 查看数据库密码
登录 宝塔 面板 

登录后点击 目录 `数据库` > 点击 按钮 `添加数据库` 填写如下信息

数据库名 iotdb utf8mb4

数据库名固定为 iotdb 

密码固定为 www.5iot.cn

选择访问权限为 所有人 (如线上环境可更具需要指定可访问IP地址)

:::


![创建数据库](../introduceImgs/docImg/createDbUser.png)




右键 点击 `新增连接 Mysql`

 ```text
填写 主机IP = 服务器ip地址

用户名默认为 root

密码为 宝塔面板复制过来的数据库密码
```


## 2.2 登录数据库

主机 填写你的服务器地址



![登录数据库](../introduceImgs/docImg/DbCreateLink.png)

右键点击 iotdb `运行SQL文件`

> 将项目文件根目录下 `sql` 文件夹 下 YunZe_0_5_0.sql 点击 运行

 
## 3 RabbitMq 配置

::: tip 登录RabbitMq
- Server-IP  = 你的 服务器 ip地址
使用浏览器打开web管理端：http://Server-IP:15672

这个时候输入 我们设置的 test test@2018 登录成功！
:::

## 3.1 新增用户

切换至 `Admin` 菜单

点击 `Add a user`  填写用户名 密码 角色权限 确认新增

角色权限选择 Admin

 ```text
Username IoTLinkRabbitMq

Password 20220107@yzIot

```

![RabbitMq新增用户](../introduceImgs/docImg/rabbitAddUser.png)


## 3.2 新增虚拟主机

切换至 `Admin` 菜单 点击 右侧 `Virtual Hosts`  填写 新增虚拟主机 名称 确认新增

 ```text
Name:  /VirtualHosts

点击 Add virtual Hosts 
```

![RabbitMq新增虚拟主机](../introduceImgs/docImg/MqHosts.png)


## 3.3 用户绑定虚拟主机

点击 `/VirtualHosts` 

![点击 /VirtualHosts ](../introduceImgs/docImg/chcikHosts.png)

选择 绑定用户为 IoTLinkRabbitMq

![点击 /VirtualHosts ](../introduceImgs/docImg/binHosts.png)

再次点击 `User`  查看是配置成功

![配置成功](../introduceImgs/docImg/MqUserSuesser.png)






## 4 Jar 包部署

 > 选空间大的盘放jar包 演示用 /mnt 举例

切换至 home目录 创建文件夹 RunJar

 ```text
cd /mnt

mkdir  RunJar

cd /mnt/RunJar
```

## 4.1 Maven打包

**添加依赖Jar**

将 `Jar` > `依赖Jar` > `yunze-apiCommin.7z` 解压 复制到 你的 maven 仓库地址 

![apiCommin_7z](../introduceImgs/docImg/yunze-apiCommin_7z.png)

 `E:\offworkApp\apache-maven-3.6.3\repository\com\yunze`  复制上 解压文件夹  `yunze-apiCommon`
 
![MavenCopyJar_apiCommont](../introduceImgs/docImg/MavenCopyJar_apiCommont.png)


找到项目 yunze

 ```text
点击 clean 清空已打包的数据 等待执行完成 

再点击 install 项目打包 等待执行完成 
``` 

![yunze-admin Jar](../introduceImgs/docImg/yunze-adminJar.png)


## 4.2 上传 Jar 包

将项目打好的Jar 包 上传至 `/mnt/RunJar`

## 4.3 启动命令

后台模式运行 jar 

::: tip 说明
- -Xms  = 启动时初始化内存 -Xms512m = 启动时初始化内存占用 512MB 

- -Xmx  = 启后最大占用内存 -Xmx1024m = 启后最大占用内存 1024MB (1G)

PS:根据服务器内存是否大于标准配置 16G 进行灵活调整 
:::

## 4.4 非首次启动

> `非首次启动`需查看当前运行的进程 将进程关闭后再执行 

 ```text
# 查看当前 java 程序执行进程 
ps -ef |grep java


# 第二个是端口号 如需关闭进程可以 将进程杀死 多个进程使用 空格 隔开

kill 进程号 进程号

# 强制杀死进程 一般情况下无需执行 在 kill 进程号 无法杀死进程时 执行
kill - 9 进程号 进程号
```

## 4.5 jar 启动
 
::: tip 首次启动注意！

首次启动时执行完 yunze-admin.jar 后 跳转至 [前端部署](#_5-1-打包-yunze-ui) 执行完再来执行 剩余其他jar 运行

【系统监控】》 (定时任务) 搜索任务名 `MQ初始化` 点击 `执行一次` 进行MQ队列初始化 

未执行 `MQ初始化` 会导致 监听轮询程序启动报错！

:::

![MqInit](../introduceImgs/docImg/MqInit.png)


 ```text

#启动前切换至 根目录
cd /

#查看当前目录
pwd

# 启动 yunze-admin.jar （PC端 主程序）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx512m -jar /mnt/RunJar/yunze-admin.jar >/dev/null 2>&1&

# 启动 cAdmin.jar（PC端-消费者 业务执行监听）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cAdmin.jar >/dev/null 2>&1&

# 启动 cardActivateDate.jar （轮询-消费者 激活时间同步业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cardActivateDate.jar >/dev/null 2>&1&

# 启动 cardDisconnected.jar（轮询-消费者 未订购停机业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cardDisconnected.jar >/dev/null 2>&1&

# 启动 cardFlow.jar（轮询-消费者 用量同步业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cardFlow.jar >/dev/null 2>&1&

# 启动 cardStatus.jar（轮询-消费者 卡状态同步业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cardStatus.jar >/dev/null 2>&1&

# 启动 cardStop.jar（轮询-消费者 达量停机业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cardStop.jar >/dev/null 2>&1&

# 启动 cOrder.jar（消费者 订单业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cOrder.jar >/dev/null 2>&1&

# 启动 cUpdate.jar（消费者 信息修改业务执行 ）
nohup  /var/java/jdk1.8.0_271/bin/java -Xms512m -Xmx1024m -jar /mnt/RunJar/cUpdate.jar >/dev/null 2>&1&


```

## 4.6 查看是否启动成功

如果 上面执行的 Jar 全在进程中说明启动成功 

 ```text
# 查看当前 java 程序执行进程 
ps -ef |grep java
```

也可查看日志是否启动成功


## 4.7 日志路径

日志默认路径为 `/mnt/yunze/logs` 该路径会随启动时路径变更 所以务必按流程启动 启动程序时 切换至根目录 `cd /`!


```
mnt.yunze.logs     
├── yunze-admin                    
│       └── yunze-admin                    
│               └── yunze-admin.log                    // 当天执行文件 过了当天命名为 yunze-admin-2021-01-07.log
├── cAdmin                    
│       └── cAdmin                    
│               └── cAdmin.log                         // 当天执行文件 过了当天命名为 cAdmin-2021-01-07.log
├── cardActivateDate                    
│       └── cardActivateDate                    
│               └── cardActivateDate.log               // 当天执行文件 过了当天命名为 cardActivateDate-2021-01-07.log
├── cardDisconnected                    
│       └── cardDisconnected                    
│               └── cardDisconnected.log               // 当天执行文件 过了当天命名为 cardDisconnected-2021-01-07.log
├── cardFlow                    
│       └── cardFlow                    
│               └── cardFlow.log                       // 当天执行文件 过了当天命名为 cardFlow-2021-01-07.log
├── cardStatus                    
│       └── cardStatus                    
│               └── cardStatus.log                     // 当天执行文件 过了当天命名为 cardStatus-2021-01-07.log
├── cardStop                    
│       └── cardStop                    
│               └── cardStop.log                       // 当天执行文件 过了当天命名为 cardStop-2021-01-07.log
├── cOrder                    
│       └── cOrder                    
│               └── cOrder.log                         // 当天执行文件 过了当天命名为 cOrder-2021-01-07.log
├── cUpdate                    
│       └── cUpdate                    
│               └── cUpdate.log                        // 当天执行文件 过了当天命名为 cUpdate-2021-01-07.log
```


## 4.8 日志查看

日志默认路径为 `/mnt/yunze/logs` 



 ```text
# 查看前切换至 根目录
cd /

# 查看当前目录
pwd

# 查看 yunze-admin.jar （PC端 主程序）
tail -f /mnt/yunze/logs/yunze-admin/yunze-admin/yunze-admin.log

# 查看 cAdmin.jar（PC端-消费者 业务执行监听）
tail -f /mnt/yunze/logs/cAdmin/cAdmin/cAdmin.log

# 查看 cardActivateDate.jar （轮询-消费者 激活时间同步业务执行 ）
tail -f /mnt/yunze/logs/cardActivateDate/cardActivateDate/cardActivateDate.log

# 查看 cardDisconnected.jar（轮询-消费者 未订购停机业务执行 ）
tail -f /mnt/yunze/logs/cardDisconnected/cardDisconnected/cardDisconnected.log

# 查看 cardFlow.jar（轮询-消费者 用量同步业务执行 ）
tail -f /mnt/yunze/logs/cardFlow/cardFlow/cardFlow.log

# 查看 cardStatus.jar（轮询-消费者 卡状态同步业务执行 ）
tail -f /mnt/yunze/logs/cardStatus/cardStatus/cardStatus.log

# 查看 cardStop.jar（轮询-消费者 达量停机业务执行 ）
tail -f /mnt/yunze/logs/cardStop/cardStop/cardStop.log

# 查看 cOrder.jar（消费者 订单业务执行 ）
tail -f /mnt/yunze/logs/cOrder/cOrder/cOrder.log

# 查看 cUpdate.jar（消费者 信息修改业务执行 ）
tail -f /mnt/yunze/logs/cUpdate/cUpdate/cUpdate.log 


```

> 退出查看 `Ctrl+c`

## 5 前端部署

## 5.1 打包 yunze-ui
打开IoTLink切换至 项目目录 `yunze-ui`

 ```text
# npm 引入包
npm i

# 构建生产环境 
npm run build:prod
```

## 5.2 上传 yunze-ui

执行完命令后 查看 `yunze-ui` 下 `dist` 目录 此目录为打包后前端界面

打开远程工具 找到 Nginx 路径，如果是按流程走到次步骤 直接切换到目录

或找到宝塔按住目录/www/server/nginx

 ```text
cd /mnt/www/server/nginx

# 创建文件夹
mkdir admin

# 切换至目录 admin
cd /mnt/www/server/nginx/admin
```

上传 至 `/mnt/www/server/nginx/admin` 



# 6 开始访问

浏览器 输入你的ip地址 或 配置的映射 域名 如 `demo.5iot.cn`

登录名 admin  密码 5iot.com

返回 [jar 启动](#_4-5-jar-启动) 



