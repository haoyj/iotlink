---
title: 数据库使用 Group By 查询报错
date: 2022-01-14
tags:
 - 说明
 - 部署
 - 帮助
 - 常见问题
categories:
 - 常见问题
---


## Group By

如果在使用过程中，发现数据库 Group By 查询报错

出现此类错误的原因是 MySQL 5.7 及以上版本，SQL Model 默认为 ONLY_FULL_GROUP_BY 。

打开宝塔面板 修改 MySQL配置

找到目录 首页 > 左下角 软件 点击 MySQL 找到 `配置修改`

主要修改为 `sql-mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES`

复制如下文件 粘贴 点击 `保存` 找到 服务 再 点击 `重启` mysql 

 ```text
[client]
#password	= your_password
port		= 3306
socket		= /tmp/mysql.sock

[mysqld]
port		= 3306
socket		= /tmp/mysql.sock
datadir = /www/server/data
default_storage_engine = InnoDB
performance_schema_max_table_instances = 400
table_definition_cache = 400
skip-external-locking
key_buffer_size = 1024M
max_allowed_packet = 100G
table_open_cache = 4096
sort_buffer_size = 16M
net_buffer_length = 4K
read_buffer_size = 16M
read_rnd_buffer_size = 256K
myisam_sort_buffer_size = 256M
thread_cache_size = 512
tmp_table_size = 512M
default_authentication_plugin = mysql_native_password
lower_case_table_names = 1
sql-mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES

explicit_defaults_for_timestamp = true
#skip-name-resolve
max_connections = 500
max_connect_errors = 100
open_files_limit = 65535

log-bin=mysql-bin
binlog_format=mixed
server-id = 1
binlog_expire_logs_seconds = 600000
slow_query_log=1
slow-query-log-file=/www/server/data/mysql-slow.log
long_query_time=3
#log_queries_not_using_indexes=on
early-plugin-load = ""

innodb_data_home_dir = /www/server/data
innodb_data_file_path = ibdata1:10M:autoextend
innodb_log_group_home_dir = /www/server/data
innodb_buffer_pool_size = 4096M
innodb_log_file_size = 2048M
innodb_log_buffer_size = 512M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
innodb_max_dirty_pages_pct = 90
innodb_read_io_threads = 8
innodb_write_io_threads = 8

[mysqldump]
quick
max_allowed_packet = 500M

[mysql]
no-auto-rehash

[myisamchk]
key_buffer_size = 1024M
sort_buffer_size = 16M
read_buffer = 2M
write_buffer = 2M

[mysqlhotcopy]
interactive-timeout

```


